﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationPanel : PanelBase
{

	[SerializeField]
	private WidgetList notificationWidgetList;

	protected override void Awake()
	{
		base.Awake();

		CheckIfSet(notificationWidgetList);
	}

	public void ShowNotification(string notification)
	{
		var newWidget = notificationWidgetList.AddWidget<NotificationWidget>();
		newWidget.Init(notification);
	}

}
