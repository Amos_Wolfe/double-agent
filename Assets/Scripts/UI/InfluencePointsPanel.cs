﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Displays the amount of influence points the player has.
/// </summary>
public class InfluencePointsPanel : PanelBase
{

	[SerializeField]
	private Text influencePointsLabel;

	protected override void Awake()
	{
		base.Awake();

		CheckIfSet(influencePointsLabel);

		gameEvents.Subscribe(GAME_EVENT.InfluencePointsChanged, this);

		OnInfluencePointsChanged(0);
	}

	public override void OnGameEvent(GAME_EVENT eventType, object args)
	{
		base.OnGameEvent(eventType, args);

		switch(eventType)
		{
			case GAME_EVENT.InfluencePointsChanged:
				OnInfluencePointsChanged((int)args);
				break;
		}
	}

	private void OnInfluencePointsChanged(int influencePoints)
	{
		if (influencePointsLabel != null)
			influencePointsLabel.text = string.Format("Influence Points: {0}", influencePoints);
	}

}
