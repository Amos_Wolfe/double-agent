﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Panel for assigning agents to a mission.
/// </summary>
public class AssignAgentsPanel : PanelBase
{

	[SerializeField]
	private Text titleLabel;
	[SerializeField]
	private WidgetList agentWidgetlist;
	[SerializeField]
	private Button startButton;
	[SerializeField]
	private Image successBar;
	[SerializeField]
	private Text influenceCostLabel;
	[SerializeField]
	private int influenceCostPerMission = 5;

	private AgentManager agentManager;
	private MissionManager missionManager;
	private MissionDetailsPanel missionDetailsPanel;
	private GameManager gameManager;

	private Mission currentMission;

	private List<Agent> assignedAgentList;

	protected override void Awake()
	{
		base.Awake();

		ReferenceManager.GetBehaviours(ref agentManager, ref missionDetailsPanel, ref missionManager);
		ReferenceManager.GetBehaviours(ref gameManager);

		CheckIfSet(titleLabel, agentWidgetlist, startButton, successBar);
		CheckIfSet(influenceCostLabel);

		if (startButton != null)
			startButton.interactable = false;

		assignedAgentList = new List<Agent>();

		gameEvents.Subscribe(GAME_EVENT.AgentAwayStatusChanged, this);

		if (influenceCostLabel != null)
			influenceCostLabel.text = string.Format("Cost: {0} Influence", influenceCostPerMission);
    }

	public override void OnGameEvent(GAME_EVENT eventType, object args)
	{
		base.OnGameEvent(eventType, args);

		switch(eventType)
		{
			case GAME_EVENT.AgentAwayStatusChanged:
				if (Visible)
					Refresh();
				break;
		}
	}

	public void Show(Mission missionInfo)
	{
		base.Show();

		this.currentMission = missionInfo;
		assignedAgentList.Clear();

		titleLabel.text = string.Format("{0}\nRequired Agents: {1}", missionInfo.Title, missionInfo.RequiredAgents);

		if(agentManager != null)
		{
			var agentList = agentManager.AgentList;
			agentWidgetlist.ClearList();

			for(int i = 0; i < agentList.Count; i++)
			{
				var currentAgent = agentList[i];
				var newWidget = agentWidgetlist.AddWidget<AgentSelectWidget>();
				newWidget.Init(currentAgent, i);
			}
        }

		successBar.fillAmount = 0;
	}

	public void Refresh()
	{
		if(currentMission != null)
			Show(currentMission);
	}

	public void OnBack()
	{
		missionDetailsPanel.Show(currentMission);
		Hide();
	}

	public void OnStartMission()
	{
		if (assignedAgentList.Count == 0) return;

		if (gameManager.InfluencePoints < influenceCostPerMission) return;
		gameManager.RemoveInfluencePoints(influenceCostPerMission);

		missionManager.StartMission(currentMission, assignedAgentList.ToArray());

		Hide();
	}

	public void OnSelectAgent(AgentSelectWidget widget)
	{
		assignedAgentList.Add(widget.AgentInfo);

		UpdateStats();
    }

	public void OnDeselectAgent(AgentSelectWidget widget)
	{
		assignedAgentList.Remove(widget.AgentInfo);

		UpdateStats();
	}

	private void UpdateStats()
	{
		startButton.interactable = assignedAgentList.Count > 0 && gameManager.InfluencePoints >= influenceCostPerMission;

		float percentageOfAgents = Mathf.Clamp01((float)assignedAgentList.Count / currentMission.RequiredAgents);
		successBar.fillAmount = currentMission.BaseSuccessChance * percentageOfAgents;
	}

}
