﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Displays the mission description with some buttons on what to do.
/// </summary>
public class MissionDetailsPanel : PanelBase
{

	[SerializeField]
	private Text titleLabel;
	[SerializeField]
	private Text descriptionLabel;

	private AssignAgentsPanel assignAgentsPanel;

	private Mission currentMission;

	protected override void Awake()
	{
		base.Awake();

		ReferenceManager.GetBehaviours(ref assignAgentsPanel);

		CheckIfSet(descriptionLabel);
	}

	public void Show(Mission missionInfo)
	{
		base.Show();

		if (this.currentMission != null && this.currentMission != missionInfo)
			gameEvents.Publish(GAME_EVENT.MissionDeselected, this.currentMission);

		this.currentMission = missionInfo;

		titleLabel.text = missionInfo.Title;
		descriptionLabel.text = missionInfo.Description;
	}

	public void OnAssignAgents()
	{
		assignAgentsPanel.Show(currentMission);
		Hide();
	}

}
