﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A BaseBehaviour that can receive and sent game events.
/// </summary>
public abstract class EventBehaviour : BaseBehaviour, IGameEventReceiver
{
	protected GameEvents gameEvents;

	protected virtual void Awake()
	{
		ReferenceManager.GetBehaviours(ref gameEvents);
	}

	public virtual void OnGameEvent(GAME_EVENT eventType, object args) { }
}
