﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Convenience methods for reading Text Assets
/// </summary>
public static class TextAssetUtils
{

	public static string[] Read(TextAsset asset)
	{
		if (asset == null) return new string[0];
		var stringArray = asset.text.Split('\n');
		return stringArray;
	}

}
