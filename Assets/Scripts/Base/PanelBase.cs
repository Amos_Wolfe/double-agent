﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PanelBase : EventBehaviour
{

	[SerializeField]
	private GameObject root;
	[SerializeField]
	private bool startActive;

	public bool Visible
	{
		get
		{
			if (root == null) return false;
			return root.activeSelf;
		}
	}

	protected override void Awake()
	{
		base.Awake();

		CheckIfSet(root);

		if (startActive)
			Show();
		else
			Hide();
	}

	public virtual void Show()
	{
		if (root != null)
			root.SetActive(true);
	}

	public virtual void Hide()
	{
		if (root != null)
			root.SetActive(false);
	}

}
