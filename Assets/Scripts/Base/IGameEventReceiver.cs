﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Receives global game events
/// </summary>
public interface IGameEventReceiver
{
    void OnGameEvent(GAME_EVENT gameEvent, object args);
}