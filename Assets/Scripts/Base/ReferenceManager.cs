﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Gets references to single class objects, and returns errors if they don't exist
/// </summary>
public class ReferenceManager
{

    public static void GetBehaviours<T>(ref T b1) 
        where T : Object
    {
        GetBehaviour(ref b1);
    }


    public static void GetBehaviours<T, U>(ref T b1, ref U b2) 
        where T : Object
        where U : Object
    {
        GetBehaviour(ref b1);
        GetBehaviour(ref b2);
    }

    public static void GetBehaviours<T, U, V>(ref T b1, ref U b2, ref V b3)
        where T : Object
        where U : Object
        where V : Object
    {
        GetBehaviour(ref b1);
        GetBehaviour(ref b2);
        GetBehaviour(ref b3);
    }

    private static void GetBehaviour<T>(ref T b) where T : Object
    {
        b = Object.FindObjectOfType<T>();
    }

    private static void GetBehaviour(ref Component b)
    {
        Object newObject = Object.FindObjectOfType(b.GetType());
        if (newObject == null)
        {
            Debug.LogErrorFormat("[ReferenceManager] Could not find object of type {0}", b.GetType());
            return;
        }
        b = newObject as Component;
    }

}
