﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MissionStatus
{
	NotStarted,
	Started,
	Success,
	Failure,
}

/// <summary>
/// Details about a mission that can be completed by a number of agents
/// </summary>
public class Mission
{
	public string Title;
	public string Description;

	/// <summary>
	/// A difficulty level between 0 and 1.
	/// </summary>
	public float DifficultyLevel;
	/// <summary>
	/// The amount of agents required to complete this mission.
	/// </summary>
	public int RequiredAgents;
	/// <summary>
	/// The location of this mission relative to the centre of the map.
	/// </summary>
	public Vector2 Location;
	/// <summary>
	/// The amount of time in seconds this mission will take
	/// </summary>
	public float Duration;

	public MissionStatus Status;

	public float BaseSuccessChance;

	public TraitEffect[] TraitEffects;
}
