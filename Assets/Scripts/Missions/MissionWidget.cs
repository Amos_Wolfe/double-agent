﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The visual representation of a mission on the map screen.
/// </summary>
public class MissionWidget : EventBehaviour
{

	private const string SELECTED_ANIMKEY = "Selected";

	[SerializeField]
	private Button missionButton;
	[SerializeField]
	private Animator animator;

	private MissionDetailsPanel missionDetailsPanel;
	private AssignAgentsPanel assignAgentsPanel;

	private Mission missionInfo;
	private bool available;

	public Mission MissionInfo { get { return missionInfo; } }

	protected override void Awake()
	{
		base.Awake();
		ReferenceManager.GetBehaviours(ref missionDetailsPanel, ref assignAgentsPanel);

		CheckIfSet(missionButton, animator);

		gameEvents.Subscribe(GAME_EVENT.MissionDeselected, this);
	}

	public override void OnGameEvent(GAME_EVENT eventType, object args)
	{
		base.OnGameEvent(eventType, args);

		switch(eventType)
		{
			case GAME_EVENT.MissionDeselected:
				OnMissionDeselected((Mission)args);
				break;
		}
	}

	public void Init(Mission missionInfo)
	{
		this.missionInfo = missionInfo;

		available = true;

		transform.localPosition = missionInfo.Location;
	}

	public void OnClicked()
	{
		if (!available) return;

		if (missionDetailsPanel != null)
			missionDetailsPanel.Show(missionInfo);
		if (assignAgentsPanel != null)
			assignAgentsPanel.Hide();

		if (animator != null)
			animator.SetBool(SELECTED_ANIMKEY, true);
    }

	/// <summary>
	/// Makes the mission not available to be clicked on.
	/// </summary>
	public void DisableMission()
	{
		available = false;
		
		if (missionButton != null)
			missionButton.interactable = false;
	}

	private void OnMissionDeselected(Mission missionInfo)
	{
		if (this.missionInfo != missionInfo) return;

		if (animator != null)
			animator.SetBool(SELECTED_ANIMKEY, false);
	}
}
