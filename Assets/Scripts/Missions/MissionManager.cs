﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates and manages the missions the player needs to do.
/// </summary>
public class MissionManager : EventBehaviour
{
	[SerializeField]
	private int missionsOnMap = 3;

	private List<Mission> currentMissionList;

	private MissionGenerator missionGen;
	protected MissionDisplayPanel missionDisplayPanel;
	private MissionResultCalculator missionResultCalculator;
	private NotificationPanel notificationPanel;

	protected override void Awake()
	{
		base.Awake();

		ReferenceManager.GetBehaviours(ref missionGen, ref missionDisplayPanel, ref missionResultCalculator);
		ReferenceManager.GetBehaviours(ref notificationPanel);

		currentMissionList = new List<Mission>();
	}

	/// <summary>
	/// Makes sure the missions on the map match the current progression level of the game.
	/// </summary>
	/// <param name="progressionLevel">The progression of the game from 0 to 1.</param>
	public void AddNewMissions(float progressionLevel)
	{
		int missionsToCreate = missionsOnMap - currentMissionList.Count;

		for (int i = 0; i < missionsToCreate; i++)
			CreateMission(progressionLevel);
	}

	/// <summary>
	/// Removes the specified mission from the map.
	/// </summary>
	public void RemoveMission(Mission mission)
	{
		int missionIndex = currentMissionList.IndexOf(mission);
		if (missionIndex == -1) return;
		currentMissionList.RemoveAt(missionIndex);

		missionDisplayPanel.RemoveMission(mission);
	}

	private void CreateMission(float progressionLevel)
	{
		Mission newMission = missionGen.CreateMission(progressionLevel);
		currentMissionList.Add(newMission);

		missionDisplayPanel.AddMission(newMission);
		
	}

	public void StartMission(Mission missionInfo, Agent[] agentList)
	{
		missionInfo.Status = MissionStatus.Started;
		missionDisplayPanel.StartMission(missionInfo, agentList);
    }

	public void CompleteMission(Mission missionInfo, Agent[] agentList)
	{
		RemoveMission(missionInfo);
		
		bool success = missionResultCalculator.CalculateSuccess(missionInfo, agentList);
		if (success)
		{
			missionInfo.Status = MissionStatus.Success;
			AddBackstories(agentList, "Completed Mission", string.Format("This agent successfully completed the mission: {0}", missionInfo.Title), false);

			if (notificationPanel != null)
				notificationPanel.ShowNotification(string.Format("Successfully Completed '{0}' Mission", missionInfo.Title));
		}
		else
		{
			missionInfo.Status = MissionStatus.Failure;
			AddBackstories(agentList, "Failed Mission", string.Format("This agent failed the mission: {0}", missionInfo.Title), true);

			if (notificationPanel != null)
				notificationPanel.ShowNotification(string.Format("Failed '{0}' Mission", missionInfo.Title));
		}

		gameEvents.Publish(GAME_EVENT.MissionCompleted, missionInfo);
	}

	private void AddBackstories(Agent[] agentList, string title, string description, bool couldBeDoubleAgent)
	{
		var newBackstory = new BackstoryEvent
		{
			Title = title,
			Description = description,
			couldBeDoubleAgent = couldBeDoubleAgent,
		};

		for (int i = 0; i < agentList.Length; i++)
			agentList[i].BackstoryList.Add(newBackstory);
	}

}
