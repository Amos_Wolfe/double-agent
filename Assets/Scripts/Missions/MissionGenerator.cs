﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct TraitEffect
{
	public PersonalityTrait Trait;
	[Tooltip("a percentage where 0 is 0% and 1 is 100%. Can go into negatives for a negative impact on success chance.")]
	public float SuccessModifier;
}

/// <summary>
/// Creates missions of a certain difficulty based on the game's progression level
/// </summary>
public class MissionGenerator : BaseBehaviour
{
	

	[System.Serializable]
	private struct MissionTemplateInfo
	{
		public string Title;
		public string Description;

		public TraitEffect[] TraitEffects;

		// TODO difficulty shown at
	}

	[SerializeField]
	private float difficultyRandomness = 0.1f;
	[SerializeField]
	private int minRequiredAgents = 1;
	[SerializeField]
	private int maxRequiredAgents = 3;
	[SerializeField]
	private float durationMin = 3;
	[SerializeField]
	private float durationMax = 10;
	[SerializeField]
	private float baseSuccessChanceMin = 0.3f;
	[SerializeField]
	private float baseSuccessChanceMax = 0.95f;

	[SerializeField]
	private MissionTemplateInfo[] missionTemplateList;

	private void Awake()
	{
		if (missionTemplateList.Length == 0)
			LogError("No Mission templates set");
	}

	public Mission CreateMission(float progressionLevel)
	{
		float difficultyLevel = progressionLevel + Random.Range(-difficultyRandomness, difficultyRandomness);
		difficultyLevel = Mathf.Clamp01(difficultyLevel);

		float agentLevel = progressionLevel + Random.Range(-difficultyRandomness, difficultyRandomness);
		int requiredAgents = minRequiredAgents + (int)((maxRequiredAgents - minRequiredAgents) * agentLevel);

		var templateChosen = missionTemplateList[Random.Range(0, missionTemplateList.Length)];

		float duration = Random.Range(durationMin, durationMax);

		float baseSuccessChance = Random.Range(baseSuccessChanceMin, baseSuccessChanceMax);

		return new Mission
		{
			Title = templateChosen.Title,
			Description = templateChosen.Description,

			DifficultyLevel = difficultyLevel,
			RequiredAgents = requiredAgents,
			Duration = duration,
			BaseSuccessChance = baseSuccessChance,
			TraitEffects = templateChosen.TraitEffects,
		};
	}

}
