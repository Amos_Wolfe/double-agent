﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Calculates the result of the mission based on the agents involved.
/// </summary>
public class MissionResultCalculator : BaseBehaviour
{

	[SerializeField, Tooltip("a percentage where 0 is 0% and 1 is 100%. Can go into negatives for a negative impact on success chance.")]
	private float doubleAgentEffect = -1;
	
	public bool CalculateSuccess(Mission missionInfo, Agent[] agentList)
	{
		float percentageOfAgents = Mathf.Clamp01((float)agentList.Length / missionInfo.RequiredAgents);

		float successChance = missionInfo.BaseSuccessChance;
		for(int i = 0; i < agentList.Length; i++)
		{
			var currentAgent = agentList[i];
			if (currentAgent.IsDoubleAgent)
				successChance += doubleAgentEffect;

			foreach(var trait in currentAgent.Traits)
			{
				int traitIndex = ReturnTraitIndex(trait, missionInfo.TraitEffects);
				if(traitIndex != -1)
				{
					var traitEffect = missionInfo.TraitEffects[traitIndex];
					successChance += traitEffect.SuccessModifier;
				}
			}
		}

		bool success = Random.Range(0.0f, 1.0f) < successChance;
		return success;
	}

	private int ReturnTraitIndex(PersonalityTrait trait, TraitEffect[] traitEffectList)
	{
		for(int i = 0; i < traitEffectList.Length; i++)
		{
			if (traitEffectList[i].Trait == trait)
				return i;
		}
		return -1;
	}

}
