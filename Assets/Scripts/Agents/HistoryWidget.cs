﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistoryWidget : BaseBehaviour
{
	[SerializeField]
	private Text titleLabel;
	[SerializeField]
	private Text descriptionLabel;

	private DosierPanel dosierPanel;

	private BackstoryEvent backstory;
	private bool configuring;
	private Toggle thisToggle;

	public BackstoryEvent Backstory { get { return backstory; } }

	private void Awake()
	{
		ReferenceManager.GetBehaviours(ref dosierPanel);

		CheckIfSet(titleLabel, descriptionLabel);

		GetComponentsWithNullCheck(ref thisToggle);
	}

	public void Init(BackstoryEvent backstory)
	{
		this.backstory = backstory;
		titleLabel.text = backstory.Title;
		descriptionLabel.text = backstory.Description;
	}

	public void OnValueChanged(bool selected)
	{
		if (configuring) return;

		if (selected)
			dosierPanel.OnHistorySelected(this);
		else
			dosierPanel.OnHistoryDeselected(this);
	}

	public void AutoDeselect()
	{
		configuring = true;

        if (thisToggle != null)
			thisToggle.isOn = false;

		configuring = false;
    }
}
