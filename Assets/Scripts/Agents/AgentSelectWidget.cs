﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Widget used to select an agent to go on a mission.
/// </summary>
public class AgentSelectWidget : AgentWidget
{

	private AssignAgentsPanel assignAgentsPanel;

	private Toggle thisToggle;

	protected override void Awake()
	{
		base.Awake();

		ReferenceManager.GetBehaviours(ref assignAgentsPanel);

		GetComponentsWithNullCheck(ref thisToggle);
	}

	public void OnValueChanged(bool selected)
	{
		if (selected)
			assignAgentsPanel.OnSelectAgent(this);
		else
			assignAgentsPanel.OnDeselectAgent(this);
    }

	public void AutoDeselect()
	{
		if (thisToggle != null)
			thisToggle.isOn = false;
	}

}
