﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PersonalityTrait
{
	None,
	Pacifist,
	Psychopath,
	Kleptomaniac,
	Honest,
	Liar,
	Subtle,
	Loud,
	MasterOfDisguise,
	Perfectionist,
	DisasterArtist,
	Loner,
	HatesFalseAccusations,
}

public enum Gender
{
	Male,
	Female
}

[System.Serializable]
public struct PersonalityInfo
{
	public PersonalityTrait Personality;
	public string Description;

	// TODO personality exclusions (e.g Psychopath cannot group with Pacifist
}

[System.Serializable]
public struct BackstoryEvent
{
	public string Title;
	public string Description;
	public PersonalityTrait[] Traits;
	public bool couldBeDoubleAgent;
}

/// <summary>
/// A secret agent that can be sent out on missions.
/// </summary>
public class Agent
{
	/// <summary>
	/// The name of this agent.
	/// </summary>
	public string Name;
	public Gender Gender;
	/// <summary>
	/// Set to true true if this agent is secretly trying to sabotage missions.
	/// </summary>
	public bool IsDoubleAgent;
	/// <summary>
	/// Determines the portrait picture this agent has.
	/// </summary>
	public int PortraitIndex;
	/// <summary>
	/// Any personality traits the agent may have.
	/// </summary>
	public PersonalityTrait[] Traits;
	/// <summary>
	/// The list of backstory events that can indicate different traits of the agent.
	/// </summary>
	public List<BackstoryEvent> BackstoryList;
	/// <summary>
	/// Set to true while this agent is away on a mission.
	/// </summary>
	public bool Away;
	/// <summary>
	/// If this reaches 1, then the agent will leave.
	/// </summary>
	public float CurrentAngerLevel;
}
